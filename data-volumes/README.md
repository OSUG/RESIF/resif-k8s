On peut déployer tous les volumes nécessaires avec Helm.

Celui-ci veut des valeurs rangées dans une map `global`. Pour cela, on récupère le fichier de référence `globals-mounts.yaml` qui est mis à disposition par intégration continue du dépôt salt https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/salt/ 

    helm install resif-archive . --namespace test --dry-run --values https://osug.gricad-pages.univ-grenoble-alpes.fr/RESIF/salt/helmvalues/global-mounts.yaml
    
L'URL pour les valeurs est le fichier de référence des points de montages pour les volumes.
