Ce répertoire contient deux helm charts:

- data-volumes: permet de déployer les configurations pour l'accès aux volumes de données dans le cluster kubernetes (un point de montage par réseau sismologique).
- ops: permet de déployer tous les programmes réalisant des opérations périodiques ou du déploiement continu

# Prérequis au déploiement

Cette recette helm suppose l'utilisation de secrets avec l'outil sops (https://github.com/getsops/sops/tree/main) version 3.8.1 ou plus.

NOTE: lorsque SOPS sortira la dernière version avec l'option `mac_only_encrypted`, on pourra fusionner les secrets et les values.yaml dans un seul fichier.

Pour le déploiement, je recommande d'utiliser le plugin helm secrets:

    helm plugin install https://github.com/jkroepke/helm-secrets
    
Cela permet de déchiffrer les secrets à la volée et de les rendre accessibles directement dans les templates.

On peut alors déployer une chart ainsi: 

    helm secrets install --values staging-secrets-values.yaml --values staging-values.yaml --namespace staging resifdc .

Sinon, on peut aussi procéder en:

  * déchiffrant manuellement les secrets avec sops
  * passer les fichiers de valeurs resultant à helm avec l'option --values

# Contextes

Les valeurs adaptées à nos contextes sont mises à disposition dans les fichiers `helm-values/<contexte>/*.yaml`.

Les secrets, quant à eux sont dans le fichier `helm-values/<contexte>/secrets.yaml`.

Pour le déploiement des volumes, on utilise un fichier de valeurs qui est généré (par intégration continue) à partir du dépôt Salt (https://osug.gricad-pages.univ-grenoble-alpes.fr/RESIF/salt/helmvalues/global-mounts.yaml).

Les fichiers de valeur peuvent servir dans le cadre du déploiement d'un "subchart", mais il faut alors fournir au subchart seulement le sous-ensemble de valeurs avec le niveau de profondeur adéquat. Pour ce faire, voir le README du subchart souhaité.

# Déploiement

## Staging

Pour les volumes:

    helm upgrade -i resif-archive ./data-volumes -f https://osug.gricad-pages.univ-grenoble-alpes.fr/RESIF/salt/helmvalues/global-mounts.yaml
   
Pour les jobs d'opération sur staging:

    helm secrets upgrade -i staging-ops ./ops -f helm-values/staging/values.yaml -f helm-values/staging/secrets.yaml

Pour les jobs d'opération sur production:

    helm secrets upgrade -i production-ops ./ops -f helm-values/production/values.yaml -f helm-values/production/secrets.yaml

## Continuous Delivery

La recette helm `ops` déploie un rôle pour les opérations de CD réalisées par exemple par la forge gitlab.

Les opérations de CD nécessitent d'utiliser un fichier de configuration contenant les paramètres de connexion et d'authentification au cluster. Ce fichier yaml peut être généré par l'admin du cluster.

Pour référence, on utilise pour cela un outil https://github.com/superbrothers/kubectl-view-serviceaccount-kubeconfig-plugin.

     kubectl view-serviceaccount-kubeconfig staging-ops-continuous-delivery
